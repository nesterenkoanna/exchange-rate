<?php
namespace app\widgets;

use yii\base\Widget;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Exception\ClientException;

class CurrentExchangeRateWidget extends Widget
{
    private $errorMessage = 'Sorry, something went wrong...';

    /**
     * @inheritdoc
     *
     * @return string
     * @throws GuzzleException
     */
    public function run()
    {
        $error = '';
        $currencies = $this->getCurrencies($error);

        return $this->render(
            'current-exchange-rate',
            [
                'currencies' => $currencies,
                'error'      => $error
            ]
        );
    }

    /**
     * Get list of currencies.
     *
     * @param string $error Error message.
     *
     * @return array
     * @throws GuzzleException
     */
    private function getCurrencies(string &$error): array
    {
        $currencies = [];
        try {
            $client = new Client();
            $response = $client->request('GET', \Yii::$app->params['exchangeUrl']);

            $body = $response->getBody();

            if ($response->getStatusCode() == 200) {
                $currencies = json_decode($body->getContents());
            } else {
                $error = $this->errorMessage;
            }
        } catch (ClientException $e) {
            $error = $this->errorMessage;
        }

        return $currencies;
    }
}
