<?php
namespace app\widgets;

use yii\base\Widget;
use app\models\CurrencyConverterForm;
use GuzzleHttp\Exception\GuzzleException;

class CurrencyConverterWidget extends Widget
{
    /**
     * @inheritdoc
     *
     * @throws GuzzleException
     */
    public function run()
    {
        $currencyConverterForm = new CurrencyConverterForm();
        return $this->render(
            'currency-converter',
            [
                'currencyConverterForm' => $currencyConverterForm,
                'currencies' => $currencyConverterForm->getCurrenciesList()
            ]
        );
    }
}
