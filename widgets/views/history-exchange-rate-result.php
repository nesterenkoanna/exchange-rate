<table class="table table-striped">
    <thead>
    <tr>
        <th scope="col">Дати</th>
        <th scope="col">Рiзниця</th>
    </tr>
    </thead>
    <tbody>
    <?php foreach ($historyExchangeRate as $date => $diff) { ?>
        <tr>
            <th><?=$date?></th>
            <td><?php echo ($diff) < 0 ? "<span class='text-danger'>$diff</span>" : $diff; ?></td>
        </tr>
    <?php } ?>
    </tbody>
</table>