<p class="font-weight-bold">Офіційний курс гривні щодо іноземних валют на дату <?=date('d.m.Y')?></p>

<?php if ($error) { ?>
    <div><?=$error;?></div>
<?php } else { ?>
    <div>
        <div class="font-weight-bold">Примітка:</div>
        <div class="font-italic">Поточного дня буде відображатися офіційний курс гривні до іноземних валют, встановлений НА ЗАВТРА за схемою:</div>
        <div class="font-italic">1. До 16:00 – відображається лише офіційний курс гривні до іноземних валют, що встановлюється 1 раз на місяць.</div>
        <div class="font-italic">2. Після 16:00 - офіційний курс, зазначений у п.1, та офіційний курс гривні до іноземних валют, що встановлюється щодня.</div>
    </div>
    <br>
    <table class="table table-striped">
        <thead>
        <tr>
            <th scope="col">Код цифровий</th>
            <th scope="col">Код літерний</th>
            <th scope="col">Назва валюти</th>
            <th scope="col">Офіційний курс</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($currencies as $currency) { ?>
            <tr>
                <th><?=sprintf("%03s", $currency->r030);?></th>
                <td><?=$currency->cc;?></td>
                <td><?=$currency->txt;?></td>
                <td><?=$currency->rate;?></td>
            </tr>
        <?php } ?>
        </tbody>
    </table>
<?php } ?>


