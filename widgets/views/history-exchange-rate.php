<?php
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use kartik\daterange\DateRangePicker;
?>
<div class="container-fluid">
    <?php
    $form = ActiveForm::begin(
        [
            'id' => 'history-exchange-rate-form',
            'validateOnBlur' => true,
            'validateOnChange' => true,
            'enableClientValidation' => true,
        ]
    );
    ?>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <?= $form
            ->field($historyExchangeRateForm, 'currency')
            ->widget(
                Select2::class,
                [
                    'data'            => $currencies,
                    'theme'           => Select2::THEME_BOOTSTRAP,
                    'options'         => [
                        'placeholder' => 'Обрати валюту',
                        'class'       => 'form-control input-sm',
                    ],
                ]
            )->label(false);
        ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <?= $form->field($historyExchangeRateForm, 'period', ['template' => '{input}'])
            ->widget(
                DateRangePicker::class,
                [
                    'convertFormat' => true,
                    'options' => [
                        'placeholder' => 'Обрати дату'
                    ],
                    'pluginOptions' => [
                        'autoclose' => true,
                        'locale' => [
                            'format' => 'd-m-Y',
                            'separator' => ' - ',
                        ],
                    ],
                ]
            )->label(false);
        ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-primary js-history-exchange-rate-button">Показати</button>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="font-weight-bold js-history-exchange-rate-result"></div>
    <div class="font-weight-bold js-history-exchange-rate-error text-danger"></div>
</div>
