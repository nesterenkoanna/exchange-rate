<?php
use yii\widgets\ActiveForm;
use kartik\select2\Select2;
use \kartik\date\DatePicker;
?>
<div class="container-fluid">
    <?php
    $form = ActiveForm::begin(
        [
            'id' => 'currency-converter-form',
            'validateOnBlur' => true,
            'validateOnChange' => true,
            'enableClientValidation' => true,
        ]
    );
    ?>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <?= $form
            ->field($currencyConverterForm, 'currency')
            ->widget(
                Select2::class,
                [
                    'data'            => $currencies,
                    'theme'           => Select2::THEME_BOOTSTRAP,
                    'options'         => [
                        'placeholder' => 'Обрати валюту',
                        'class'       => 'form-control input-sm',
                    ],
                ]
            )->label(false);
        ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <?= $form->field($currencyConverterForm, 'date', ['template' => '{input}'])
            ->widget(
            DatePicker::class,
            [
                'type' => DatePicker::TYPE_INPUT,
                'options' => [
                    'placeholder' => 'Обрати дату'
                ],
                'pluginOptions' => [
                    'autoclose' => true,
                    'format'    => 'dd-mm-yyyy',
                ],
            ]
            )->label(false);
        ?>
    </div>

    <div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
        <button type="button" class="btn btn-primary js-currency-converter-button">Показати</button>
    </div>

    <?php ActiveForm::end(); ?>

    <div class="font-weight-bold js-currency-converter-result text-success"></div>
    <div class="font-weight-bold js-currency-converter-error text-danger"></div>
</div>
