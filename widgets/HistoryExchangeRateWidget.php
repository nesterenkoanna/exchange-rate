<?php
namespace app\widgets;

use yii\base\Widget;
use app\models\HistoryExchangeRateForm;
use GuzzleHttp\Exception\GuzzleException;

class HistoryExchangeRateWidget extends Widget
{
    /**
     * @inheritdoc
     *
     * @throws GuzzleException
     */
    public function run()
    {
        $historyExchangeRateForm = new HistoryExchangeRateForm();
        return $this->render(
            'history-exchange-rate',
            [
                'historyExchangeRateForm' => $historyExchangeRateForm,
                'currencies' => $historyExchangeRateForm->getCurrenciesList()
            ]
        );
    }
}
