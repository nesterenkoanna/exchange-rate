'use strict';
let main = {

    page: 'main',
    action: null,
    config: null,
    gettingConfigStarted: false,

    init: function ()
    {
        this.initMenu();
        this.currencyConverter();
        this.historyExchangeRate();
    },

    initMenu: function ()
    {
        let that = this;
        $('.js-page').on('click', function() {
            that.clearMenu();
            that.page = $(this).data('page');
            that.action = $(this).data('action');

            $('.js-menu li').removeClass('active');
            $(this).addClass('active');

            $('.page-container').addClass('hide');

            that.activatePage();
        });
    },

    clearMenu: function () {
        this.page = null;
        this.action = null;
    },

    activatePage: function () {
        switch (this.page) {
            case 'current-exchange-rate':
                $('.page-current-exchange-rate').removeClass('hide');
                break;
            case 'history-exchange-rate':
                $('.page-history-exchange-rate').removeClass('hide');
                break;
            case 'currency-converter':
                $('.page-currency-converter').removeClass('hide');
                break;

        }
    },

    currencyConverter: function() {
        let currencyConverterButton = $('.js-currency-converter-button');
        let currencyConverterError = $('.js-currency-converter-error');
        let currencyConverterResult = $('.js-currency-converter-result');

        currencyConverterButton.on('click', function() {
            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: 'site/currency-converter',
                data: $('#currency-converter-form').serialize(),
                beforeSend: function() {
                    currencyConverterButton.prop('disabled', true);
                    currencyConverterError.text('');
                    currencyConverterResult.text('');
                },
                success: function (response) {
                    if (response.error.length != 0) {
                        currencyConverterError.text(response.error);
                    } else {
                        currencyConverterResult.text(response.rate);
                    }

                    currencyConverterButton.prop('disabled', false);
                }
            });
        });
    },

    historyExchangeRate: function() {
            let historyExchangeRateButton = $('.js-history-exchange-rate-button');
            let historyExchangeRateError = $('.js-history-exchange-rate-error');
            let historyExchangeRateResult = $('.js-history-exchange-rate-result');

            historyExchangeRateButton.on('click', function() {
                $.ajax({
                    dataType: 'json',
                    type: 'POST',
                    url: 'site/history-exchange-rate',
                    data: $('#history-exchange-rate-form').serialize(),
                    beforeSend: function() {
                        historyExchangeRateButton.prop('disabled', true);
                        historyExchangeRateError.text('');
                        historyExchangeRateResult.text('');
                    },
                    success: function (response) {
                        if (response.error.length != 0) {
                            historyExchangeRateError.text(response.error);
                        } else {
                            historyExchangeRateResult.html(response.content);
                        }

                        historyExchangeRateButton.prop('disabled', false);
                    }
                });
            });
        }
};
main.init();