<?php

use app\widgets\CurrencyConverterWidget;
use app\widgets\CurrentExchangeRateWidget;
use app\widgets\HistoryExchangeRateWidget;

/* @var $this yii\web\View */

$this->title = 'Exchange rate';
?>
<nav class="navbar navbar-expand-lg navbar-light bg-light js-menu">
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="js-page active" data-page="current-exchange-rate">
                <a class="nav-link" href="javascript:void(0)">Current exchange rate</a>
            </li>
            <li class="js-page" data-page="history-exchange-rate">
                <a class="nav-link" href="javascript:void(0)">History exchange rate</a>
            </li>
            <li class="js-page" data-page="currency-converter">
                <a class="nav-link" href="javascript:void(0)">Currency converter</a>
            </li>
        </ul>
    </div>
</nav>

<div class="page-container page-current-exchange-rate">
    <?= CurrentExchangeRateWidget::widget(); ?>
</div>
<div class="page-container page-history-exchange-rate hide">
    <?= HistoryExchangeRateWidget::widget(); ?>
</div>
<div class="page-container page-currency-converter hide">
    <?= CurrencyConverterWidget::widget(); ?>
</div>