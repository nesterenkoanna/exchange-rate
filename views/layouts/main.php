<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use app\assets\MainAsset;
use yii\bootstrap4\BootstrapAsset;

MainAsset::register($this);
BootstrapAsset::register($this);
?>
<?php $this->beginPage() ?>
    <!DOCTYPE HTML PUBLIC>
    <html lang="<?= Yii::$app->language ?>">
        <head>
            <meta charset="<?= Yii::$app->charset ?>">
            <meta http-equiv="X-UA-Compatible" content="IE=edge">
            <meta name="viewport" content="width=device-width, initial-scale=1">
            <?= Html::csrfMetaTags() ?>
            <title><?= Html::encode($this->title) ?></title>
            <script type="text/javascript" language="javascript" src="//code.jquery.com/jquery-1.12.4.min.js"></script>
            <![endif]-->
            <?php $this->head() ?>
        </head>
        <body>
            <?php $this->beginBody() ?>

            <div class="page-layout">
                <div class="page-content">
                    <header>
                    </header>
                    <!-- Header ends -->
                    <div class="wrapper">
                        <?= $content; ?>
                    </div>
                </div>
            </div>

            <?php $this->endBody() ?>
        </body>
    </html>
<?php $this->endPage() ?>