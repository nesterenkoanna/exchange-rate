<?php

return [
    'bsDependencyEnabled' => false,
    'exchangeUrl' => 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?json',
    'dateRateUrl' => 'https://bank.gov.ua/NBUStatService/v1/statdirectory/exchange?valcode=%s&date=%s&json',
];
