<?php
namespace app\models;

use \DateTime;
use \DatePeriod;
use \DateInterval;
use yii\base\Model;
use app\traits\CurrencyConverterTrait;
use GuzzleHttp\Exception\GuzzleException;

/**
 * HistoryExchangeRateForm.
 */
class HistoryExchangeRateForm extends Model
{
    use CurrencyConverterTrait;

    const DATE_SEPARATOR = ' - ';

    public $currency;
    public $period;
    public $error;

    private $errorMessage = 'Sorry, something went wrong...';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency', 'period'], 'required'],
        ];
    }

    /**
     * Get rate by currency and date.
     *
     * @return mixed|null
     * @throws GuzzleException
     */
    public function getHistoryExchangeRate()
    {
        $dateSeparate = explode(self::DATE_SEPARATOR, $this->period);

        $begin = new DateTime($dateSeparate[0]);
        $end   = new DateTime($dateSeparate[1]);
        $end   = $end->modify( '+1 day' );

        $interval = DateInterval::createFromDateString('1 day');
        $period = new DatePeriod($begin, $interval, $end);

        $results = [];
        foreach ($period as $dt) {
            $dateRateFromNBU = $this->sendRequestForCurrencyDateRate($dt->format('Ymd'));

            if ($dateRateFromNBU) {
                $results[] = [
                    'date' => $dt->format('d-m-Y'),
                    'rate' => $dateRateFromNBU[0]->rate
                ];
            } else {
                $this->error = $this->errorMessage;
                return null;
            }
        }

        $historyExchangeRate = [];
        foreach ($results as $i => $result) {
            if (isset($results[$i+1])) {
                $rateDiff = $results[$i+1]['rate'] - $result['rate'];
                $historyExchangeRate[$results[$i+1]['date'] . ' - ' . $result['date']] = $rateDiff;
            } else {
                $historyExchangeRate[$result['date']] = $result['rate'];
            }
        }

        return array_reverse($historyExchangeRate);
    }
}
