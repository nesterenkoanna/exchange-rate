<?php
namespace app\models;

use yii\base\Model;
use app\traits\CurrencyConverterTrait;
use GuzzleHttp\Exception\GuzzleException;

/**
 * CurrencyConverterForm.
 */
class CurrencyConverterForm extends Model
{
    use CurrencyConverterTrait;

    public $currency;
    public $date;
    public $error;

    private $errorMessage = 'Sorry, something went wrong...';

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['currency', 'date'], 'required'],
        ];
    }

    /**
     * Get rate by currency and date.
     *
     * @return mixed|null
     * @throws GuzzleException
     */
    public function getRate()
    {
        $dateRateFromNBU = $this->sendRequestForCurrencyDateRate(date('Ymd', strtotime($this->date)));

        if ($dateRateFromNBU) {
            return $dateRateFromNBU[0]->rate;
        } else {
            $this->error = $this->errorMessage;
            return null;
        }
    }
}
