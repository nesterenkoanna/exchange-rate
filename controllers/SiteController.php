<?php
namespace app\controllers;

use \Yii;
use yii\web\Response;
use yii\web\Controller;
use app\models\CurrencyConverterForm;
use app\models\HistoryExchangeRateForm;
use GuzzleHttp\Exception\GuzzleException;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays change rate page.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Get currency rate by currency and date.
     *
     * @return string[]
     * @throws GuzzleException
     */
    public function actionCurrencyConverter(): array
    {
        $this->layout = false;
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['error' => ''];

        $form = new CurrencyConverterForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $response['rate'] = $form->getRate();

            if (!empty($form->error)) {
                $response['error'] = $form->error;
            }
        } else {
            $errors = $form->getFirstErrors();
            $response['error'] = reset($errors);
        }

        return $response;
    }

    /**
     * Get history exchange rate rate by currency and period.
     *
     * @return string[]
     * @throws GuzzleException
     */
    public function actionHistoryExchangeRate(): array
    {
        $this->layout = false;
        Yii::$app->response->format = Response::FORMAT_JSON;

        $response = ['error' => ''];

        $form = new HistoryExchangeRateForm();
        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $historyExchangeRate = $form->getHistoryExchangeRate();

            if (!empty($form->error)) {
                $response['error'] = $form->error;
            } else {
                $response['content'] = $this->renderAjax(
                    '@widgets/views/history-exchange-rate-result',
                    [
                        'historyExchangeRate' => $historyExchangeRate,
                    ]
                );
            }
        } else {
            $errors = $form->getFirstErrors();
            $response['error'] = reset($errors);
        }

        return $response;
    }
}
