<?php
namespace app\traits;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\GuzzleException;

/**
 * CurrencyConverterForm.
 */
trait CurrencyConverterTrait
{
    /**
     * Send request for currency date rate.
     *
     * @param string $date Date for NBU.
     *
     * @return array
     * @throws GuzzleException
     */
    protected function sendRequestForCurrencyDateRate(string $date): array
    {
        $dateRateFromNBU = [];
        try {
            $client = new Client();
            $response = $client->request(
                'GET',
                sprintf(\Yii::$app->params['dateRateUrl'], $this->currency, $date)
            );

            $body = $response->getBody();

            if ($response->getStatusCode() == 200) {
                $dateRateFromNBU = json_decode($body->getContents());
            } else {
                $this->error = $this->errorMessage;
            }
        } catch (ClientException $e) {
            $this->error = $this->errorMessage;
        }

        return $dateRateFromNBU;
    }

    /**
     * Get currencies list.
     *
     * @return array
     * @throws GuzzleException
     */
    public function getCurrenciesList(): array
    {
        $currenciesFromNBU = $this->sendRequestForExchangeRate();

        $currencies = [];
        foreach ($currenciesFromNBU as $currencyFormNBU) {
            $currencies[$currencyFormNBU->cc] = $currencyFormNBU->cc;
        }

        return $currencies;
    }

    /**
     * Send request for exchange rate.
     *
     * @return array
     * @throws GuzzleException
     */
    protected function sendRequestForExchangeRate(): array
    {
        $currenciesFromNBU = [];
        try {
            $client = new Client();
            $response = $client->request('GET', \Yii::$app->params['exchangeUrl']);

            $body = $response->getBody();

            if ($response->getStatusCode() == 200) {
                $currenciesFromNBU = json_decode($body->getContents());
            }
        } catch (ClientException $e) {
            $this->error = $this->errorMessage;
        }

        return $currenciesFromNBU;
    }
}
